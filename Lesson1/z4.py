# Write a Python program to count the number of strings where the string length is 2 or more and the first and last character are same from a given list of strings.
# Sample List : ['abc', 'xyz', 'aba', '1221']
# Expected Result : 2
l = list(input('Введите значения через пробел ').split())
n = 0
for i in l:
    if i[:1] == i[-1:] and len(i) > 2:
        n = n + 1
print(l)
print(n)