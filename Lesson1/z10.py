# Write a Python program to get the difference between the two lists
l1 = list(input().split())
l2 = list(input().split())
print(list(set(l1) - set(l2)))