# Write a Python program to count the number of characters (character frequency) in a string.
# Sample String : google.com
# Expected Result : {'o': 3, 'g': 2, '.': 1, 'e': 1, 'l': 1, 'm': 1, 'c': 1}
s = input()
d = {}
for i in s:
    n = 0
    for j in s:
        if i == j:
            n = n + 1
            d.update({i:n})
        else:
            d.update({i:n})
print(d)
