# 1. You are asked to ensure that the first and last names of people begin with a capital letter in their passports.
# For example, alison heck should be capitalized correctly as Alison Heck.
# Given a full name, your task is to capitalize the name appropriately.
# Input Format:A single line of input containing the full name, S.
# Constraints:* 0 < len(S) < 1000*
# The string consists of alphanumeric characters and spaces.
# Note: in a word only the first character is capitalized.
# Example 12abc when capitalized remains 12abc.
# Output Format:Print the capitalized string, S.
s = input()
if 0 < len(s) < 1000:
    for i in s:
        if i == ' ':
            s2 = s[s.index(' ')+1:(s.index(' '))+2:1].upper()
            s = s[:1].upper() + s[1:s.index(' '):1] + " " + s2 + s[(s.index(' '))+2:len(s):1]
    else:
        s = s[:1].upper() + s[1:len(s):1]
print(s)
# не понимаю, как сделать с неограниченным количеством пробелов :с