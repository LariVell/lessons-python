# You are given with 3 sets, find if third set is a subset of the first and the second sets
# Input: set([1,2]), set([2,3), set([2])
# Expected result: True
# Input: set([1,2]), set([3,4), set([5])
# Expected result: False
l1 = set(input('Введите значения через пробел ').split())
l2 = set(input('Введите значения через пробел ').split())
l3 = set(input('Введите значения через пробел ').split())
bol = False
for i in l1:
    for j in l2:
        if i == j:
            bol = True
        else:
            bol = False
        for k in l3:
            if j == k:
                bol = True
            else:
                bol = False
print(bol)