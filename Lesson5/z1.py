# Разработать класс Complex, которые бы описывал комплексные числа, позволял их
# складывать, вычитать, умножать, делить и получать модуль.
# Вывод действительной и мнимой частей должен быть с точностью до двух знаков после
# запятой.
# На вход: действительная и мнимая часть числа, разделенная пробелом.
#
# На выходе:
# Для двух комплексных чисел вывод должен быть в следующей последовательности в
# отдельных строках:
# C + D
# C – D
# C * D
# C/D
# mod(C)
# mod(D)


class Complex(complex):
    def __init__(self, number):
        super().__init__()
        self.number = number

    def __add__(self, other):
        return self.number + other

    def __sub__(self, other):
        return self.number - other

    def __mul__(self, other):
        return self.number * other

    def __div__(self, other):
        return self.number / other


def enter():
    number = list(input().split())
    return complex(int(number[0]), int(number[1]))


first_number = Complex(enter())
second_number = Complex(enter())

print(first_number + second_number)
print(first_number - second_number)
print(first_number * second_number)
print(first_number / second_number)
print(abs(first_number))
print(abs(second_number))
