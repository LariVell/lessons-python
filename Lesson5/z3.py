# Написать базовый класс Observable, который бы позволял наследникам:
# при передаче **kwargs заносить соответствующие значения как атрибуты
# сделать так, чтобы при print отображались все публичные атрибуты
# >>> class X(Observable):
# ... pass
# >>> x = X(foo=1, bar=5, _bazz=12, name=&#39;Amok&#39;, props=(&#39;One&#39;, &#39;two&#39;))
# >>> print x
# X(bar=5, foo=1, name='Amok', props=('One','two'))
# >>> x.foo
# >>> x.name
# 'Amok'
# >>> x._bazz

class X_dict(dict):

    def __getattr__(self, item):
        return self[item]

x_dick = X_dict(foo="name", bar=5, _bazz=2)
print(x_dick._bazz)
