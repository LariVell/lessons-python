# Написать свою имплементацию функции range() из Python 2.x (аналогично Python 3, только возвращает список).
def ran(a):
    l = []
    i = 0
    while i < a:
        l.append(i)
        i += 1
    return l


n = int(input())
print(ran(n))
