def chain(*args):
    for arg in args:
        for x in arg:
            yield x


l1 = iter([1, 2, 3, 4])
l2 = iter([1, 2, 3, 4, 5, 6])
a = chain(l1, l2)
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
