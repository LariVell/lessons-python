def cycle(x):
    lis = []
    for element in x:
        lis.append(element)
        yield element
    n = 0
    while True:
        yield lis[n]
        n += 1
        if n >= len(lis):
            n = 0


i = iter([1, 2, 3, 4, 5])
a = cycle(i)
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
