import string


def func(element):
    if element[:2] == "__":
        return False
    else:
        return True


print(list(filter(func, [atribut for atribut in dir(string)])))
