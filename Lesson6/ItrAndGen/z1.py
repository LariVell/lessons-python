class EvenIterator(list):

    def __init__(self, l):
        super().__init__()
        self.l = l
        self.i = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.i % 2 == 0:
            self.i += 1
            return self.l[self.i - 1]
        else:
            self.i += 1


items = list(input().split())

item = EvenIterator(items)
print(next(item))
print(next(item))
print(next(item))
print(next(item))
print(next(item))
