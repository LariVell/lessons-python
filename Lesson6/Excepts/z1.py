a = input()

if 0 <= int(a) <= 10:
    for i in range(int(a)):
        a = list(input().split())
        try:
            b = int(a[0])/int(a[1])
            print(int(b))
        except ZeroDivisionError:
            print("Error Code: integer division or modulo by zero")
        except ValueError as e:
            print("Error Code:", e)
else:
    print("no diaposon")