# Каррирование.
#   Преобразовать вызов функции с конечным количеством позиционных аргументов f(a, b, c, d) в вызов вида f(a)(b)(c)(d), используя декоратор.
#   Пример:
#   @carry
#   def foo(a, b):
#         return a + b
#
#    foo(1)(5)  # вернет 6
def carry(func):
    def curried(*args, **kwargs):
        if len(args) + len(kwargs) >= func.__code__.co_argcount:
            return func(*args, **kwargs)
        return (lambda *args2, **kwargs2:
                curried(*(args + args2), **dict(kwargs, **kwargs2)))
    return curried

@carry
def foo(a, b):
    return a + b


print(foo(1)(5))  # вернет 6
