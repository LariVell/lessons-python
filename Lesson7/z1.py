# Напишите параметризованный декоратор, который считает и выводит при каждом вызове среднее время работы функции за n последних вызовов.
#    Время выводить в миллисекундах.
#   Пример использования:
#
#   @average_time(n=2)
#   def foo(a):
#       sleep(a)
#       return a
#
#   foo(3) # вернет 3 и выведет сообщение "Среднее время работы: 3000 мс."
#   foo(7) # вернет 7 и выведет сообщение "Среднее время работы: 5000 мс."
#   foo(1) # вернет 1 и выведет сообщение "Среднее время работы: 4000 мс."
# не так наверно
from time import sleep, time


def average_time(n=1):
    def time_of_function(function):
        def wrapper(*args, **kwargs):
            time_first = time()
            res = function(*args, **kwargs)
            print(f'Среднее время работы: {((time() - time_first) / n)*1000} мс.')
            return res
        return wrapper
    return time_of_function


@average_time(n=2)
def foo(a):
    sleep(a)
    return a

assert foo(3)
assert foo(7)
assert foo(1)
