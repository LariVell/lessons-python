# Написать функцию, которая будет принимать только 4 позиционных аргументы (все аргументы числовые).
#     Функция должна вернуть среднее арифметическое аргументов и самый большой аргумент за все время вызовов этой функции.
#     Пример: foo(1,2,3,4) -> 2.5, 4
#                   foo(-3, -2, 10, 1) -> 1.5, 10
#                   foo(7,8,8,1) -> 6, 10
def func(number1, number2, number3, number4):
    ari = (number1 + number2 + number3 + number4) / 4
    maxi = max(number1, number2, number3, number4)
    if func.maxi < maxi:
        func.maxi = maxi
    return ari, func.maxi


func.maxi = 0
print(func(1, 2, 3, 4))
print(func(-3, -2, 10, 1))
print(func(7, 8, 8, 1))
