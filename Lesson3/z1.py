# Написать несколько функций, которые в качестве единственного аргумента принимают список (или кортеж) целых чисел.
#      - первая функция должна вернуть квадраты элементов коллекции;
#      - вторая функция должна вернуть только элементы на четных позициях;
#      - третья функция возвращает кубы четных элементов на нечетных позициях.
#
list_input = [1, 2, 3, 4, 5]
square_list = list(map(lambda element: element * element, list_input))
print(square_list)


def chet(list_numbers):
    list_chet = []
    for number in range(len(list_numbers)):
        if number % 2 == 0:
            list_chet.append(list_numbers[number])
    return list_chet


print(chet(list_input))


def nec(list_numbers):
    list_nec = []
    for number in range(len(list_numbers)):
        if number % 2 != 0:
            list_nec.append(list_numbers[number])
    list_cube = list(map(lambda element: element * element * element, list_nec))
    return list_cube


print(nec(list_input))
