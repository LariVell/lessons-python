# Написать функцию, которая принимает произвольное количество любых аргументов.
#     Аргументами могут быть вложенные списки и кортежи, содержащие числа и другие списки и кортежи.
#     Пример вызова функции: foo(1, 2, [3, 4, (5, 6, 0)], a=(10, 11), b=(3, 4, [5, 6, [7, 8], []]))
#     Функция должна вернуть произведение и сумму всех ненулевых элементов вложенных чисел.
#     Возможны циклические ссылки в аргументах. Пример такого аргумента: a = [1, 2, 3]; a.append(a)
#     При обнаружении циклической ссылки нужно сообщить пользователю и вернуть None.
# не очень красивая, но я пыталась
# не поняла с циклической ссылкой


def func(*args, **kwargs):
    sum = 0
    pro = 1
    list_str = ', '.join(map(str, args)) + ', '

    for value in kwargs.values():
        list_str = list_str + ', '.join(map(str, value)) + ', '

    list_str = list_str.replace("[", "").replace("]", "").replace("(", "").replace(")", "").replace("[]", "")

    list_number = []
    for number in list_str.split(', '):
        if number.isdigit():
            list_number.append(number)

    for number in list_number:
        if int(number) != 0:
            sum = sum + int(number)
            pro = pro * int(number)
    return sum, pro


print(func(1, 2, [3, 4, (5, 6, 0)], a=(10, 11), b=(3, 4, [5, 6, [7, 8], []])))
